﻿var epaApiFactory = ["$http", function ($http) {
    var factory = {};
    var queryBuilder = [];

    var url = "/api/Epa/query";
    var queryStruct = {
        tableName: null,
        columnName: null,
        operator: null,
        columnValue: null
    }
    var output = "JSON";

    factory.results = null;

    factory.getDefaultParams = function () {
        return queryStruct;
    }

    factory.AddParameter = function (params) {
        var result = angular.extend(queryStruct, params);
        var paramBuilder = [];
        if (!result.tableName) {
            throw "No table name provided.";
        }

        paramBuilder.push(result.tableName);

        if (result.columnName) {
            paramBuilder.push(result.columnName);
        }
        
        if (result.operator) {
            paramBuilder.push(result.operator);
        }

        if (result.columnValue) {
            paramBuilder.push(result.columnValue);
        }
        return paramBuilder;
    }

    var buildQuery = function (parameters, start, count) {
        queryBuilder = [];
        queryBuilder.push.apply(queryBuilder, parameters);
        queryBuilder.push("rows");
        queryBuilder.push(start.toString() + ":" + count.toString());
        queryBuilder.push(output);
        return queryBuilder.join("/");
    };

    factory.CallApi = function (parameters) {
        var query = buildQuery(parameters, 0, 25);
        return $http.post(url, JSON.stringify(query))
            .then(function (result) {
                factory.results = result;
                return factory.results;
            },
            function (error) {
                return error;
            });
    }

    return factory;
}];