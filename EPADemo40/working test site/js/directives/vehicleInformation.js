﻿var vehicleInformationDirective = function () {
    return {
        restrict: "E",
        scope: {
            "myData": "="
        },
        link: function (scope, element, attrs, documentController) {
        	scope.change = function (o) {
            	scope.myData({ make: o.make, model: o.model, year: o.year, hybridity: o.hybridity });
        	};
        },
        templateUrl: "/partials/directives/vehicleInformation.html"
    };
}