﻿var distanceCalculatorDirective = function () {
    return {
        restrict: "E",
        scope: {
            "myData": "="
        },
        link: function (scope, element, attrs, documentController) {
            scope.change = function (o) {
                scope.myData({ origin: o.origin, destination: o.destination });
            };
        },
        templateUrl: "/partials/directives/distanceCalculator.html"
    };
};