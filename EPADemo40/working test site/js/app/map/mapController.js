﻿var mapController = ["$scope", "$sce", "brighterPlanet", "googleDirections", "enviroFacts", function ($scope, $sce, brighterPlanet, googleDirections, enviroFacts) {
    $scope.processing = false;
    $scope.active = false;
    $scope.shippingQuery = {};

    $scope.sanitize = function (val) {
        return $sce.trustAsHtml(val);
    }

    $scope.extendShippingData = function (data) {
        $scope.shippingQuery = angular.extend($scope.shippingQuery, data);
    }

    $scope.setData = function (d) {
        $scope.data = d;
    }

    $scope.setDirections = function (d) {
        $scope.directions = d;
    }

    $scope.setEpaData = function (d) {
        $scope.epaData = d;
    }

    $scope.clickMe = function () {
        var postData = $scope.shippingQuery;
        $scope.active = $scope.processing = true;
    	googleDirections.Locations(postData.origin, postData.destination)
            .then(function (locations) {
                postData.distance = Math.round(googleDirections.CalculateDistance(locations) / 100) / 10;

                brighterPlanet.CallApi("shipments", postData)
                    .then(function (result) {
                        $scope.setData(result);
                        $scope.processing = false;
                    },
                    function (error) {
                        $scope.processing = false;
                        $scope.active = false;
                        console.log("Unable to get shipment data.");
                    });

                googleDirections.GetDirections(locations[0], locations[1])
                    .then(function (dirResult) {
                        $scope.setDirections(dirResult);
                        console.log("Directions returned.");
                    }
                    , function () {
                        console.log("Unable to get directions.");
                    });

                enviroFacts.CallApi([enviroFacts.AddParameter({ tableName: "t_design_for_environment" })])
                    .then(function (epaResult) {
                        $scope.setEpaData(epaResult);
                    },
                    function (error) {
                        console.log("Unable to get Envirofactor data.");
                    });
            },
            function () {
                $scope.processing = false;
                $scope.active = false;
                console.log("Service is unavailable.");
            });
    }
}];
