﻿var worldMapServiceFn = ["$q", function ($q) {
    var Map = {
        defaults: {
            center: new google.maps.LatLng(38.4987789, -98.035841),
            zoom: 4,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        },
        el: null,
        instance: null,
        isPrinting: false,
        markers: [],
        locations: [],
        geoLocations: [],
        data: [],
        index: 0,
        delay: 100,
        failures: 0,
        openWindow: {},
        heatMap: {},
        state: "",
        addressProgress: 0.0,
        addressLookupComplete: false,
        updateProgress: function () { },
        initialize: function (el) {
            var me = this;
            this.el = el;
            this.instance = new google.maps.Map(el, this.defaults);
            this.geocoder = new google.maps.Geocoder();
            this.map_bounds = new google.maps.LatLngBounds();
            this.directionsService = new google.maps.DirectionsService();
			//Keep this null and load when needed. Trying new approach. May convert others if this works well; Let's try to limit what we use on the client.
            this.directionsRenderer = null; 
        },
        calculateDistance: function(originLatLng, destinationLatLng) {
            return google.maps.geometry.spherical.computeDistanceBetween(originLatLng, destinationLatLng);
        },
        getDirections: function (originLatLng, destinationLatLng) {
            var me = this;
            var postData = {
                origin: originLatLng,
                destination: destinationLatLng,
                travelMode: google.maps.TravelMode.DRIVING
            };

            if (!me.directionsRenderer) {
                me.directionsRenderer = new google.maps.DirectionsRenderer();
            }

            me.directionsRenderer.setMap(me.instance);

            return $q(function (resolve, reject) {
                me.directionsService.route(postData, function (result, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        me.directionsRenderer.setDirections(result);
                        resolve(result);
                        return result;
                    } else {
                        reject(status);
                        return status;
                    }
                });
            });
        },
        storeAddressLocations: function () {
            var me = this;
            if (me.index < me.locations.length) {
                setTimeout(function () {
                    me.addressToLocation(me.locations[me.index]);
                }, me.delay);
            } else {
                me.addressLookupComplete = true;
            }
        },
        geoCodeAddresses: function (addresses) {
            var me = this;
            me.reset();
            me.locations = addresses;
            var tid = 0;
            return $q(function (resolve, reject) {
                if (me.index < 0 || me.locations.length < me.index) {
                    reject("Out of bounds.");
                }

                me.storeAddressLocations();
                tid = setInterval(function () {
                    if (me.addressLookupComplete) {
                        clearInterval(tid);
                        resolve(me.geoLocations);
                        return me.geoLocations;
                    }
                }, 250);
            });
        },
        addressToLocation: function (address) {
            var me = this;
            me.geocoder.geocode({
                'address': address
            }, function (response, status) {
                switch (status) {
                    case google.maps.GeocoderStatus.OK:
                        me.geoLocations.push(response[0].geometry.location);
                        me.index++;
                        me.addressProgress = me.index / me.locations.length;
                        break;
                    case google.maps.GeocoderStatus.OVER_QUERY_LIMIT:
                        me.delay++;
                        me.failures++;
                        break;
                    default:
                        me.delay++;
                        me.failures++;
                        console.log("Geocode failed because: " + status);
                        break;
                }
                me.storeAddressLocations();
            });
        },
    //displayHeatAddress: function () {
    //    var me = this;
    //    if (me.index < me.data.results.length) {
    //        setTimeout(function () {
    //            me.codeAddress(me.data.results[me.index].term + ', ' + me.state);
    //        }, me.delay);
    //    } else {
    //        me.heatMap = new google.maps.visualization.HeatmapLayer({
    //            data: me.locations,
    //            radius: 20
    //        });
    //        me.heatMap.setMap(me.instance);
    //        me.instance.fitBounds(me.map_bounds);
    //    }
    //},
    //    var marker = new google.maps.Marker({
    //        position: location //,
    //        //title: joinFirmNames(parentBlock)
    //    });
    //marker.setMap(me.instance);
    //me.markers.push(marker);
    //var infobox = new google.maps.InfoWindow({
    //    content: parentBlock.html()
    //});
    //google.maps.event.addListener(marker, 'click', function () {
    //    if (me.openWindow.close != undefined) me.openWindow.close();
    //    me.openWindow = infobox;
    //    infobox.open(me.instance, this);
    //});
    //getGridRow(address).css({ cursor: "pointer" }).on("click", function () {
    //    var idx = $.inArray($(this).text(), Map.locations);
    //    if (idx !== -1) {
    //        google.maps.event.trigger(Map.markers[idx], 'click');
    //    }
    //});
    ////Used in heatmap code.
    //var loc = response[0].geometry.location;
    //me.locations.push({ location: loc, weight: Math.max(me.data.results[me.index].count * 2.25, 5) });
    //me.index++;
    //me.map_bounds.extend(loc);
    //me.updateProgress((me.index / me.data.results.length) * 100);
        clear: function () {
            this.clearMarkers();
        },
        clearMarkers: function () {
            for (var i = 0, len = this.markers.length; i < len; i++) {
                var marker = this.markers[i];
                marker.setMap(null);
            }
            this.markers = [];
            return this;
        },
        reset: function () {
            var me = this;
            me.data = [];
            me.addressProgress = 0.0;
            me.index = 0;
            me.delay = 100;
            me.geoLocations = [];
            me.locations = [];
            me.addressLookupComplete = false;
            me.map_bounds = new google.maps.LatLngBounds();

            if (me.directionsRenderer) {
                me.directionsRenderer.setMap(null);
            }

            if (me.heatMap.setMap != undefined) {
                me.heatMap.setMap(null);
            }
        },
        start: function (data) {
            var me = this;
            me.reset();
            me.clearMarkers();
            me.data = data;
            me.index = 0;
            //me.locations = $.unique($.map(data.results, function (value) {
            //    return value.city + ', ' + value.state;
            //}));
        },
        setLocations: function (locations) {
            var me = this;
            me.locations = locations;
        },
        setData: function (data) {
            var me = this;
            me.data = data;
        }
    };

    this.Map = Map;
}];