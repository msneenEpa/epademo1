﻿var googleDirectionsFactory = ["$http", "worldMapService",
    function ($http, worldMapService) {
        var factory = {};
        factory.results = null;

        var setKey = function (param) {
            param.key = key;
            return param;
        }

        factory.GetDirections = function (origin, destination) {
            return worldMapService.Map.getDirections(origin, destination);
        };

        factory.CalculateDistance = function (locations) {
            return worldMapService.Map.calculateDistance(locations[0], locations[1]);
        };

        factory.Locations = function (origin, destination) {
            return worldMapService.Map.geoCodeAddresses([origin, destination])
                .then(function (results) {
                    factory.results = results;
                    return factory.results;
                },
                function (error) {
                    console.log("ERROR");
                });
        };

        return factory;
    }];