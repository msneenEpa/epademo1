﻿var bpApiFactory = ["$http",
    function ($http) {
        var factory = {};
        var url = "http://impact.brighterplanet.com/"; 

        factory.results = null;

        factory.CallApi = function (areaName, params) {
            params.key = "737e5f4d703080f5d8eb755bd4bcdf78";
            return $http.post([url, areaName, ".json"].join(""), params)
                .then(
                function (result) {
                    factory.results = result.data;
                    return factory.results;
                },
                function (error) {
                    console.log("ERROR!");
                });
            
        }

        return factory;
    }];