﻿// Write your Javascript code.

function getChildElementByName(element, name) {
    var result = null, children = element[0].children;
    if (!children) return result;
    for (var i = 0, l = children.length; i < l; i++) {
        if (children[i].name !== name) continue;
        result = children[i];
    }
    return result;
}
