﻿//737e5f4d703080f5d8eb755bd4bcdf78 --Brighter planet key
document.addEventListener('DOMContentLoaded', function () {
	var app = angular.module("app", ["ngRoute", "ngSanitize", "chart.js"]);

    //Routes.
    app.config(function ($locationProvider, $routeProvider) {
        $locationProvider.html5Mode({ enabled: true, requireBase: false });
        $routeProvider
            .when('/', {
            	templateUrl: '/partials/trip/index.html',
            	controller: 'tripCtrl'
            })
            .otherwise({ redirectTo: '/' });
    })
	.config(['ChartJsProvider', function (ChartJsProvider) {
		ChartJsProvider.setOptions({
			colours: ["#9ed03b", "#ff953f", "#5c9ad0", "#5c9ad0"],
			responsive: false,
			legend: true,
			series: ["Car", "Plane", "Train"]
		});
	}])
    .factory("brighterPlanet", bpApiFactory)
    .factory("enviroFacts", epaApiFactory)
    .factory("googleDirections", googleDirectionsFactory)
    .directive("distanceCalculator", distanceCalculatorDirective)
    .directive("shipping", shippingDirective)
    .directive("vehicleInfo", vehicleInformationDirective)
    .directive("worldmap", worldMapDirective)
    .service("worldMapService", worldMapServiceFn)
    .controller('homeCtrl', homeController)
    .controller('mapCtrl', mapController)
	.controller('tripCtrl', tripController);
    
    angular.bootstrap(document, ["app"]);
});
