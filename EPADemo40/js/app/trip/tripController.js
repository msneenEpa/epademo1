﻿var tripController = ["$scope", "$sce", "$interval", "brighterPlanet", "googleDirections", function ($scope, $sce, $interval, brighterPlanet, googleDirections) {
	$scope.directions = null;
	$scope.drivingData = null;
	$scope.flightData = null;
	$scope.trainData = null;
	$scope.tripQuery = {};
	$scope.comparisons = [];
	$scope.dataProgress = 0;
	$scope.decisions = {
		car: [],
		train: [],
		plane: []
	};

	$scope.setTab = function (v) {
		$scope.tab = v.label;
	}

	//Allows Math to be used in {{ }} expresions.
	$scope.Math = Math;

	function storeComparison(data) {
		if ($scope.comparisons.length === 0) {
			for (item in data.equivalents) {
				$scope.comparisons.push({ key: item.replace(/_/g, ' '), value: item });
			}
			$scope.compared = $scope.comparisons[0];
		}
	}

	function startsWith(obj) {
		var include = ["carbon", "ch4", "co2", "hfc", "n2o", "energy", "ghg", "diesel"], result = false;
		for (var i = 0, l = include.length; i < l; i++) {
			result = obj.indexOf(include[i]) >= 0 && obj.indexOf("factor") === -1;
			if (result === true) break;
		}
		return result;
	}

	function AddDecision(decision, d, label) {
		for (item in d) {
			if (startsWith(item)) {
				decision.push({ key: item, value: d[item], label: label });
			}
		}
	}

	function resetCharts() {
		$scope.loaded = false;
		$scope.breakdown = [];
		$scope.chartData = [];
		$scope.chartLabels = [];
		$scope.decisions = {
			car: [],
			train: [],
			plane: []
		};
	}

	$scope.extendTripQuery = function (data) {
		$scope.tripQuery = angular.extend({}, $scope.tripQuery, data);
	};

	function extractValues(arr, label, icon) {
		for (item in arr) {
			if (item === $scope.compared) {
				var result = { label: label, value: arr[item], icon: icon };
				$scope.breakdown.push(result);
				$scope.chartLabels.push(result.label);
				$scope.chartData.push(result.value);
				break;
			}
		}
	}
	
	function extractDecision(arr) {
		for (item in arr) {

		}
	}

	$scope.iconClass = function (v) {
		return v.icon;
	}

	function addAllDecisions() {
		AddDecision($scope.decisions.car, $scope.drivingData.decisions, "Car");
		AddDecision($scope.decisions.train, $scope.trainData.decisions, "Train");
		AddDecision($scope.decisions.plane, $scope.flightData.decisions, "Plane");
	}

	function extractAll() {
		extractValues($scope.drivingData.equivalents, "Car", "fa-car car-color");
		extractValues($scope.flightData.equivalents, "Plane", "fa-plane plane-color");
		extractValues($scope.trainData.equivalents, "Train", "fa-train train-color");
	}

	$scope.onSelected = function () {
		resetCharts();
		extractAll();
		addAllDecisions();
		$scope.loaded = true;
	}

	$scope.normalize = function (v) {
		return v.replace(/_/g," ");
	}

	$scope.calculate = function () {
		var postData = $scope.tripQuery;
		resetCharts();
		googleDirections.Locations(postData.origin, postData.destination)
			.then(function (locations) {
				//Direct distance between the two points.
				postData.distance = Math.round(googleDirections.CalculateDistance(locations) / 100) / 10;

				googleDirections.GetDirections(locations[0], locations[1])
                    .then(function (directions) {
                    	$scope.directions = directions;
                    	return directions;
                    }
                    , function () {
                    	console.log("Unable to get directions.");
                    });

				//Now we have google results.
				//Get more detailed results from google. 
				//Set speed to 60mph avg.
				//postData.duration = directions.routes[0].legs[0].duration;
				//postData.distance = Math.round(directions.routes[0].legs[0].distance / 100) / 10;
				postData.speed = 96.6; //60 mph for cars.
				brighterPlanet.CallApi("automobile_trips", postData)
					.then(function (autoTrip) {
						$scope.drivingData = autoTrip;
						storeComparison(autoTrip);
						$scope.dataProgress++;
						console.log(autoTrip);
					},
					function (error) {
						console.log("Unable to get auto trip data.");
					});

				brighterPlanet.CallApi("flights", { distance_estimate: postData.distance })
					.then(function (flightTrip) {
						$scope.flightData = flightTrip;
						$scope.dataProgress++;
					},
					function (error) {
						console.log("Unable to get flight data.");
					});

				brighterPlanet.CallApi("rail_trips", { distance: postData.distance })
                    .then(function (trainTrip) {
                    	$scope.trainData = trainTrip;
						$scope.dataProgress++;
                    },
                    function (error) {
                    	console.log("Unable to get train trip data.");
                    });

				$scope.intervalId = $interval(function () {
					if ($scope.dataProgress / 3 == 1) {
						$scope.compared = "cars_off_the_road_for_a_year";
						extractAll();
						addAllDecisions();
						$interval.cancel($scope.intervalId);
						$scope.dataProgress = 0;
					}
				}, 1000);
				$scope.loaded = true;
			},
			function (error) {
				console.log("Error getting location in calculate.");
			});

	}
}];