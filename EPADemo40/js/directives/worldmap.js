﻿var worldMapDirective = ['$window', 'worldMapService', function ($window, worldMapService) {
    return {
        restrict: "E",
        link: function(scope, element, attrs, documentController) {
            if ($window.google && $window.google.maps) {
                console.log("maps loaded.");
                worldMapService.Map.initialize(document.getElementById("map"));
            }
        },
        templateUrl: "/partials/directives/worldMap.html"
    };
}];