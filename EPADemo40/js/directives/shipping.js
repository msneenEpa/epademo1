﻿var shippingDirective = function () {
    return {
        restrict: "E",
        scope: {
            "myData": "="
        },
        link: function (scope, element, attrs, documentController) {
            scope.change = function (o) {
                scope.myData({ carrier: o.carrier, mode: o.mode, weight: o.weight, package_count: o.package_count });
            };
        },
        templateUrl: "/partials/directives/shipping.html"
    };
}